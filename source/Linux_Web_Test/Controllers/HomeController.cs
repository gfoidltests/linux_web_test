﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Linux_Web_Test.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            Dictionary<object, object> properties = new Dictionary<object, object>();

            AddValues(typeof(IIdentity), this.User.Identity, BindingFlags.Instance | BindingFlags.Public);

            var connection = this.ControllerContext.HttpContext.Connection;
            AddValues(connection.GetType(), connection, BindingFlags.Instance | BindingFlags.Public);

            AddValues(typeof(HttpRequest), this.Request, BindingFlags.Instance | BindingFlags.Public);
            AddHeaders();

            AddValues(typeof(Environment), ignoreList: new string[] { "StackTrace", "NewLine" });
            AddValues(typeof(AppContext));

            AddValues(typeof(System.Runtime.GCSettings));
            AddValues(typeof(System.Runtime.InteropServices.RuntimeInformation));

            Assembly assembly = Assembly.GetEntryAssembly();
            AddValues(typeof(Assembly), assembly, BindingFlags.Instance | BindingFlags.Public, new string[] { "ExportedTypes", "DefinedTypes", "CustomAttributes", "ReflectionOnly", "Modules", "EntryPoint" });

            return View(properties);
            //-----------------------------------------------------------------
            void AddValues(Type type, object target = null, BindingFlags flags = BindingFlags.Static | BindingFlags.Public, params string[] ignoreList)
            {
                var pis = type.GetProperties(flags).AsEnumerable();

                if (ignoreList != null)
                    pis = pis.Where(p => !ignoreList.Contains(p.Name));

                foreach (var pi in pis)
                    try
                    {
                        properties[$"{type.Name} - {pi.Name}"] = pi.GetValue(target);
                    }
                    catch { }
            }
            //-----------------------------------------------------------------
            void AddHeaders()
            {
                foreach (var header in this.Request.Headers)
                    properties[$"Header - {header.Key}"] = header.Value;
            }
        }
    }
}