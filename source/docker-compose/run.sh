#!/bin/bash

docker-compose up -d
docker-compose scale www-app=2

echo
echo Containers are running. To stop them enter 'docker-compose down'
echo
